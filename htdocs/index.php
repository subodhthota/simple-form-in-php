<!DOCTYPE html>
<html>
<head>
    <title>TASK 1 </title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>


<div class="container">
    <h3>TASK 1 , SIMPLE FORM WITH TEXT AREAS ,CHECK BOX AND DROP DOWN BOX</h3>
    <form action="pro.php" method="POST">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Age:</label>
            <input type="number" name="age" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Department:</label>
                 <select name="dept">

								  <option value="">Select...</option>
								
								  <option value="CSE">Computer Science</option>
								
								  <option value="ECE">ECE</option>
								  <option value="EEE">EEE</option>
								  <option value="MECH">Mechanical</option>
								  <option value="CIV">Civil</option>
					 </select>

        </div>
        <div class="form-group">
            <input type="checkbox" name="formCheck" value="Yes" />
            <label>I accept Terms and Conditions</label>
               
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">Submit</button>
        </div>
    </form>
</div>


</body>
</html>